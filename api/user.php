<?php

require_once __DIR__ . '/../lib/db.php';
require_once __DIR__ . '/../config.php';

if (empty($_GET['id']) || empty($_GET['token'])) {
    header('HTTP/1.1 400');
    die;
}

$token = hash_hmac(HMAC_ALGO, 'test1234' . $_SERVER['REMOTE_ADDR'], HMAC_SALT);

if (hash_equals($token, $_GET['token'])) {
    $dbh = connect();
    $user = $dbh->prepare("SELECT * FROM user WHERE idU = ?");
    $user->execute([$_GET['id']]);
    $data = $user->fetchObject();
    echo json_encode($data);
}