<?php

// Todo : Créez une fonction générique permettant d'effectuer des cURL vers n'importe quel endpoint, via des méthodes GET, PUT, POST ou DELETE et avec n'importe quels paramètres.
// Les options cURL les plus courantes devront être possibles via cette fonction.
// Cette fonction devra également gérer les erreurs et les codes HTTP.
// Appelez ensuite cette fonction pour atteindre les différents endpoints de notre api.

require_once __DIR__ . '/../config.php';

function curl(string $endpoint, string $method = 'GET', array $params = [], bool $json = true)
{
    $host = 'http://localhost/webservices';
    $url = $host . $endpoint;

    $options = [];

    if ($method == 'GET') {
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }
    } elseif ($method == 'POST') {
        $options[CURLOPT_POSTFIELDS] = $params;
    }

    $ch = curl_init();
    $options[CURLOPT_URL] = $url;
    $options[CURLOPT_CUSTOMREQUEST] = $method;
    if ($json) {
        $options[CURLOPT_HEADER] = 'Content-Type: application/json';
        $options[CURLOPT_RETURNTRANSFER] = true;
    }
    curl_setopt_array($ch, $options);

    $output = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($output === false || curl_errno($ch)) {
        return 'Erreur Curl : ' . curl_error($ch);
    } elseif ($http_code != 200) {
        header('HTTP/1.1 ' . $http_code);
        die;
    } else {
        return $output;
    }
}

$token = hash_hmac(HMAC_ALGO, 'test1234' . $_SERVER['SERVER_ADDR'], HMAC_SALT);
$new_user = curl('/api/createUser.php', 'POST', ['name' => 'curl_client3', 'email' => 'curl_client3@yolo.com', 'password' => 'test', 'token' => $token]);
$get_user = json_decode($new_user);
echo curl('/api/user.php', 'GET', ['id' => $get_user->idU, 'token' => $token]);
